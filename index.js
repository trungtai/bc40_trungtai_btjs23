// BAI 1: TÍNH TIỀN LƯƠNG NHÂN VIÊN
function tinhTien() {
    var congMotNgay = 100000;
    var ngayCong = document.getElementById("ngayCong").value * 1;
    var tinhLuong = (congMotNgay * ngayCong);
    document.getElementById('result').innerHTML = `Tổng lương là: ${tinhLuong} VNĐ`;
}

// BÀI 2: TÍNH GIÁ TRỊ TRUNG BÌNH
function tinhToan() {
    var soA = document.getElementById("nhapSoA").value * 1;
    var soB = document.getElementById("nhapSoB").value * 1;
    var soC = document.getElementById("nhapSoC").value * 1;
    var soD = document.getElementById("nhapSoD").value * 1;
    var soE = document.getElementById("nhapSoE").value * 1;
    var thuchienPhepTinh = (soA + soB + soC + soD + soE) / 5;
    document.getElementById('result2').innerHTML = `Giá trị trung bình là ${thuchienPhepTinh}`;
}

// BAI 3: QUI ĐỔI TIỀN
function quiDoiTien() {
    var giaMotUsd = 23500;
    var soLuongTien = document.getElementById("soLuongTien").value * 1;
    var quiDoi = (giaMotUsd * soLuongTien);
    document.getElementById('result3').innerHTML = `Qui đổi được là: ${quiDoi} VNĐ`;
}

// BÀI 4: TÍNH DIỆN TÍCH, CHU VI HÌNH CHỮ NHẬT
function phepTinh() {
    var chieuDai = document.getElementById("chieuDai").value * 1;
    var chieuRong = document.getElementById("chieuRong").value * 1;
    var chuVi = (chieuDai + chieuRong) * 2;
    var dienTich = chieuDai * chieuRong;
    document.getElementById("result4").innerHTML = `Chu vi la ${chuVi}, Dien tich la ${dienTich}`;

}

// BÀI 5: TÍNH TỔNG HAI KÝ SỐ
function tinhHaiKySo() {
    var kySoA = document.getElementById("kySoA").value * 1;
    var kySoB = document.getElementById("kySoB").value * 1;
    var numberA = kySoA % 10;
    var numberB = kySoB % 10;
    var tinhTongHaiKySo = numberA + numberB;
    document.getElementById("result5").innerHTML = `Tổng hai ky số la ${tinhTongHaiKySo}`;
}



